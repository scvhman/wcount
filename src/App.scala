object Main extends App {
  println(countWords(args.toList))
}

object countWords extends (List[String] => Int) {
  override def apply(v1: List[String]): Int = {
    v1.map(word => word.toLowerCase).filter(word => "[a-z]".r.findAllIn(word).mkString == word).toSet.size
  }
}